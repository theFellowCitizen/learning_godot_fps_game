extends KinematicBody

const MOUSE_SENSITIVIT = 0.2
const MOVE_SPEED = 3

const JUMP_FORCE: float = 3.0
const GRAVITITY_ACCELERATION = 9.8

onready var look_pivot = $LookPivot #Get Node "LookPivot"

var input_move: Vector3 = Vector3()
var gravity_local: Vector3 = Vector3()

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	pass # Replace with function body.

func _input(event):
	if event is InputEventMouseMotion:
		rotate_y(deg2rad (-1 * event.relative.x * MOUSE_SENSITIVIT))
		look_pivot.rotate_x(deg2rad (event.relative.y * MOUSE_SENSITIVIT))
		look_pivot.rotation.x = clamp(look_pivot.rotation.x , deg2rad(-90), deg2rad(90))

func _physics_process(delta):
	input_move = get_input_direction() * MOVE_SPEED
	if not is_on_floor():
		gravity_local += GRAVITITY_ACCELERATION * Vector3.DOWN * delta
	else:
		gravity_local = GRAVITITY_ACCELERATION * -get_floor_normal() * delta
		
	if Input.is_action_just_pressed("jump") and is_on_floor():
		gravity_local = Vector3.UP * JUMP_FORCE
		
	move_and_slide(input_move + gravity_local, Vector3.UP)
	
func get_input_direction() -> Vector3:
	var z: float = (
		Input.get_action_strength("forward") - Input.get_action_strength("back")
	)
	var x: float = (
		Input.get_action_strength("left") - Input.get_action_strength("right")
	)
	return transform.basis.xform(Vector3(x,0,z).normalized()) # Calculate relative Player position to global  
